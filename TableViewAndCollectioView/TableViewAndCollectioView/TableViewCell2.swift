//
//  TableViewCell2.swift
//  TableViewAndCollectioView
//
//  Created by KMSOFT on 19/04/17.
//  Copyright © 2017 KMSOFT. All rights reserved.
//

import UIKit

class TableViewCell2: UITableViewCell , UICollectionViewDelegate, UICollectionViewDataSource {

    
    @IBOutlet var collectionView: UICollectionView!
    let array = ["1.png","2.png","3.png","4.png","1.png","2.png","3.png","4.png","2.png","1.png"]
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.dataSource = self
        collectionView.delegate = self
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell2", for: indexPath) as! CollectionViewCell2
        
        cell.myLabel.text = array[indexPath.item]
        
        return cell
    }


}
